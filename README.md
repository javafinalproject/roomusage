# README #

# Advanced Java Final Project

* For CIS-2288
* Project Members: Jon Beharrel, Logan Noonan, Philip Parke

## Description
The final project will be an application which meets a real life business need.  Each group will be assigned a project.  The business requirements must be obtained prior to development.  The groups will have flexibility in their design but the design must meet the business need.  

Although the project will be a group project, there will be components assigned to each member which will require course concepts to be applied by each learner.  The project is to follow the common look and feel for the CIS projects and the programming standards.

## Deliverables

**Project Proposal Document**
*10 Points*

Due: October 6, 2014 *

The submission for this is the design document with section 1 completed.  This will be updated in future submissions as the design is completed.

**Project Initial Design Document**
*15 Points*

Due: October 27, 2014

This document will describe the functionality of the application. This will include detailed description of the functionality, user interface design, and a description of the classes to be used.

**Project Presentation**
*? Points*

Due: December 10, 2014

A description of the background of the project and a demonstration of the solution. This will be limited to 30minutes each. Questions/Answers from class.

**Project Solution**
*75 Points*

Due: December 12, 2014

A java web application is to be developed to meet the needs specified above.
