
CREATE DATABASE roomusage;

CREATE USER 'roomusage'@'localhost' IDENTIFIED BY '123';

GRANT DELETE ON roomusage.* TO 'roomusage'@'localhost';
GRANT INSERT ON roomusage.* TO 'roomusage'@'localhost';
GRANT SELECT ON roomusage.* TO 'roomusage'@'localhost';
GRANT UPDATE ON roomusage.* TO 'roomusage'@'localhost';

USE roomusage;

CREATE TABLE users(
  id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  username VARCHAR(30) NOT NULL UNIQUE,
  firstname VARCHAR(30) NOT NULL,
  lastname VARCHAR(30) NOT NULL,
  password VARCHAR(128) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE rooms(
  id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  room_number VARCHAR(255) NOT NULL,
  campus VARCHAR(255) NOT NULL,
  room_type VARCHAR(255) NOT NULL,
  seats INT NOT NULL,
  description VARCHAR(5000) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE sensors(
  id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  sensor_identifier VARCHAR(255) NOT NULL,
  room_id BIGINT UNSIGNED,
  FOREIGN KEY (room_id) REFERENCES rooms(id) ON DELETE SET NULL,
  PRIMARY KEY (id)
);

CREATE TABLE bookings(
  id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  booked_from DATETIME NOT NULL,
  booked_to DATETIME NOT NULL,
  booked_by VARCHAR(255) NOT NULL,
  description VARCHAR(5000) NOT NULL,
  room_id BIGINT UNSIGNED,
  FOREIGN KEY (room_id) REFERENCES rooms(id) ON DELETE SET NULL,
  PRIMARY KEY (id)
);

CREATE TABLE usages(
  id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  used_from DATETIME NOT NULL,
  used_to DATETIME NOT NULL,
  room_id BIGINT UNSIGNED,
  FOREIGN KEY (room_id) REFERENCES rooms(id) ON DELETE SET NULL,
  PRIMARY KEY (id)
);

CREATE TABLE reports(
  id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  report_type VARCHAR(255) NOT NULL,
  start_date TIMESTAMP NOT NULL,
  end_date TIMESTAMP NOT NULL,
  room_id BIGINT UNSIGNED,
  total_booking_time VARCHAR(32) NOT NULL,
  total_usage_time VARCHAR(32) NOT NULL,
  usage_percentage VARCHAR(32) NOT NULL,
  FOREIGN KEY (room_id) REFERENCES rooms(id) ON DELETE SET NULL,
  PRIMARY KEY (id)
);

CREATE TABLE sessions (
  id      BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  user_id BIGINT UNSIGNED NOT NULL,
  token   VARCHAR(255)    NOT NULL,
  expires DATETIME       NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users (id),
  PRIMARY KEY (id)
);
