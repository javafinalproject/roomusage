import com.cis2232.roomusage.Application;
import com.cis2232.roomusage.AuthService;
import com.cis2232.roomusage.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.SimpleJsonParser;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import org.apache.tomcat.util.codec.binary.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

/**
 * Author: Philip Parke
 * Date: December 1, 2014
 * For: CIS-2232
 * Room Usage Tests
 * Test class
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class RoomUsageTests {

    @Autowired
    AuthService authService;

    RestTemplate restTemplate = new TestRestTemplate();


    /**
     * Test for token generation upon login
     * Requirements: A user with the username 'user'
     * and a password 'password' must exist in the
     * database, application should be available at
     * http://localhost:8080
     * @throws Exception
     */
    @Test
    public void testToken() throws Exception {

        // Create necessary Authorization headers
        HttpHeaders loginHeaders = new HttpHeaders();
        String loginString = "Basic " + new String(Base64.encodeBase64("user:password".getBytes()));
        loginHeaders.set("Authorization", loginString);

        // Send request to the server
        ResponseEntity response = restTemplate.exchange("http://localhost:8080/api/v1/login", HttpMethod.GET, new HttpEntity<Object>(loginHeaders), String.class);

        // Get the response as a string
        String tokenJson = response.getBody().toString();

        // Make sure it contains a token key
        assertThat(tokenJson, containsString("token"));

        SimpleJsonParser parser = new SimpleJsonParser();

        // Parse into a map
        Map<String,Object> tokenMap = parser.parseMap(tokenJson);

        // Make sure the returned token isn't empty
        // this would result from the user not successfully
        // logging in
        assertNotSame("", tokenMap.get("token"));

        // Create Basic Authorization string to use with verifyToken
        String authString = "Basic " + new String(Base64.encodeBase64(("user:" + tokenMap.get("token").toString()).getBytes()));

        // verifyToken should return true if the token is the same as
        // the one stored in the sessions table of the database for this user
        assertTrue(authService.verifyToken(authString));

    }
}
