package com.cis2232.roomusage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Author: Philip Parke
 * Date: November 17, 2014
 * For: CIS-2232
 * Session Controller
 * Controller for the Session model
 * Responsible for REST routes to create, access, and verify sessions.
 */
@RestController
@RequestMapping(value="/api/v1")
public class SessionController {

    @Autowired
    SessionRepository repository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthService authService;


    /**
     * Get Token
     * Provide the server with login credentials via basic authorization
     * if they are valid, the server will respond with a new token
     * and update the entry for the user in the sessions table.
     */
    @RequestMapping(value="/login", method=RequestMethod.GET)
    public Map<String,String> login(@RequestHeader(value="Authorization") String auth) {

        Map<String, String> tokenMap = new HashMap<String, String>();
        tokenMap.put("token", "");
        String token = "";

        // Split into username and password
        String parts[] = authService.decodeBasicAuth(auth);

        // Get the user with that username
        User user = userRepository.findByUsername(parts[0]);

        // If the user was found
        if(user != null) {
            try {
                // Attempt to validate the password
                if (PasswordHash.validatePassword(parts[1], user.getPassword())) {
                    // Generate a new token if successful
                    tokenMap = authService.generateToken();
                    token = tokenMap.get("token");

                    // Get the current date-time in mysql format
                    Calendar cal = Calendar.getInstance();
                    // Expire in one day
                    cal.add(Calendar.DAY_OF_MONTH, 1);
                    // Mysql format
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    // Get string
                    String expires = sdf.format(cal.getTime());

                    // Store the token in the sessions table
                    Session session = repository.findByUserId(user.getId());

                    // If the user has a session entry, update it with the new
                    // token and expire date
                    if(session != null){
                        session.update(token, expires);
                        repository.save(session);
                    }
                    // Otherwise create a new entry
                    else{
                        Session newSession = new Session(user.getId(), token, expires);
                        repository.save(newSession);
                    }
                }
            } catch (NoSuchAlgorithmException ex) {
                System.out.println("Fatal Error: Algorithm not found.\n" + ex);
                System.exit(1);

            } catch (InvalidKeySpecException ex) {
                System.out.println("Fatal Error: Invalid key spec.\n" + ex);
                System.exit(1);
            }
        }

        // Token will be empty if unsuccessful
        return tokenMap;
    }

    /**
     * Validate Token
     * Provide the server with login credentials via basic authorization
     * if they are valid, the server will respond with a new token
     * and update the entry for the user in the sessions table.
     */
    @RequestMapping(value="/validate", method=RequestMethod.GET)
    public Map<String,String> validate(@RequestHeader(value="Authorization") String token) {
        Map<String,String> response = new HashMap<>();
        response.put("status", "");

        if(authService.verifyToken(token)){
            response.put("status", "OK");
            return response;
        }else{
            return response;
        }
    }



}
