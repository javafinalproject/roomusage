package com.cis2232.roomusage;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Author: Philip Parke
 * Date: November 26, 2014
 * For: CIS-2232
 * NotAuthorizedException
 *
 * Used to return a 401 unauthroized response.
 */
@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class NotAuthorizedException extends RuntimeException{
}
