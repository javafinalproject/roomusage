package com.cis2232.roomusage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Author: Philip Parke/Logan Noonan
 * Date: November 25, 2014
 * For: CIS-2232
 * Room Controller
 * Controller for the Report model
 * Responsible for REST routes to access reports.
 */
@RestController
@RequestMapping(value="/api/v1")
public class ReportController {

    @Autowired
    CrudRepository<Report, Long> repository;

    @Autowired
    AuthService authService;

    @Autowired
    BookingRepository bookingRepository;

    @Autowired
    UsageRepository usageRepository;

    /**
     * Index
     * Show all reports.
     * @return
     */
    @RequestMapping(value="/reports", method=RequestMethod.GET)
    public List<Report> index(@RequestHeader(value="Authorization") String token)
    {
        if(authService.verifyToken(token)){

            List<Report> reports_list = new ArrayList<Report>();
            for (Report item : repository.findAll()) {
//                item.setUsageStatistics(generateUsageReport(item.getRoomId(), item.getStartDate(), item.getEndDate()));
                reports_list.add(item);
            }
            return reports_list;
        }
        else
            throw new NotAuthorizedException();
    }

    /**
     * Show
     * Show a specified report.
     * @param id
     * @return the report that was found
     */
    @RequestMapping(value="/reports/{id}", method=RequestMethod.GET)
    public Report show(@PathVariable long id, @RequestHeader(value="Authorization") String token) {

        if(authService.verifyToken(token)){
            Report report = repository.findOne(id);
//            report.setUsageStatistics(generateUsageReport(report.getRoomId(), report.getStartDate(), report.getEndDate()));
            return report;
        }
        else
            throw new NotAuthorizedException();
    }

    /**
     * Store
     * Create a new report.
     * @param report
     * @return the report that was created
     */
    @RequestMapping(value="/reports", method=RequestMethod.POST)
    public Report store(@RequestBody Report report, @RequestHeader(value="Authorization") String token)
    {
        if(authService.verifyToken(token)){
            report.setUsageStatistics(generateUsageReport(report.getRoomId(), report.getStartDate(), report.getEndDate()));
            Report newReport = repository.save(report);

            return newReport;
        }
        else
            throw new NotAuthorizedException();
    }

    /**
     * Update
     * Update an existing report.
     * @param report
     * @return the location of the report as a uri.
     */
    @RequestMapping(value="/reports/{id}", method=RequestMethod.POST, headers = "Accept=application/json")
    public Map<String,String> update(@PathVariable long id, @RequestBody Report report, @RequestHeader(value="Authorization") String token)
    {
        Map<String,String> response = new HashMap<>();
        response.put("status", "");

        if(authService.verifyToken(token)) {
            // Get the existing user
            Report existingReport = repository.findOne(id);
            existingReport.setUsageStatistics(generateUsageReport(report.getRoomId(), report.getStartDate(), report.getEndDate()));
            // Update the room
            existingReport.update(report.getReportType(), report.getStartDate(), report.getEndDate(), report.getRoomId(), report.getTotalBookingTime(), report.getTotalUsageTime(), report.getUsagePercentage());

            // Save the changes in the DB
            repository.save(existingReport);
        }else{
            throw new NotAuthorizedException();
        }

        return response;

    }

    /**
     * Delete
     * Delete an existing report.
     * @param id
     * @return
     */
    @RequestMapping(value="/reports/{id}", method=RequestMethod.DELETE)
    public Map<String,String> delete(@PathVariable long id, @RequestHeader(value="Authorization") String token)
    {
        Map<String,String> response = new HashMap<>();
        response.put("status", "");

        if(authService.verifyToken(token)) {
            // Delete the room
            repository.delete(id);

            response.put("status", "OK");
        }else{
            throw new NotAuthorizedException();
        }

        return response;
    }

    /**
     * Generates a usage report for each booking within the specified time.
     *
     * @param reportRoomId
     * @param reportStartDate
     * @param reportEndDate
     * @return report
     */
    public UsageReport generateUsageReport(String reportRoomId, String reportStartDate, String reportEndDate) {
        //report parameters
        String fromDate = reportStartDate;
        String toDate = reportEndDate;
        //the actual report
        UsageReport report = new UsageReport();
        //one report is produced for each booking within the start and end dates
        //each report has an array list of usages that fall within that booking
        List<Usage> reportUsages = new ArrayList<Usage>();
        //finds all usages within the date range

        List<Usage> usages = usageRepository.findByUsedFromBetweenAndUsedToBetween(fromDate, toDate, fromDate, toDate);

        //finds all bookings within the date range
        List<Booking> bookings = bookingRepository.findByBookedFromBetweenAndBookedToBetween(fromDate, toDate, fromDate, toDate);

        double bookingTotalTime = 0;
        double usageTime = 0;
        //loop through each booking
        for (Booking thisBooking : bookings) {
            //define the date time format used by the sql
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
            //get booking start date and end date and store as a calendar object
            Calendar startDate = Calendar.getInstance();
            Calendar endDate = Calendar.getInstance();
            try {
                startDate.setTime(sdf.parse(thisBooking.getBookedFrom()));
                endDate.setTime(sdf.parse(thisBooking.getBookedTo()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            //get the room id for this booking
            long roomId = thisBooking.getRoomId();
            //ensure that the booking matches the report room id
            if (roomId == Long.parseLong(reportRoomId)) {
                //determine total booking time length
                bookingTotalTime += ((endDate.getTimeInMillis() - startDate.getTimeInMillis()) / 1000);

                for (Usage thisUsage : usages) {
                    //                //ensure that the usage matches the selected room
                    if (roomId == thisUsage.getRoomId()) {
                        //create calendar objects to store the usage times
                        Calendar usageStart = Calendar.getInstance();
                        Calendar usageEnd = Calendar.getInstance();
                        //store the usage start and usage end as Calendar objects
                        try {
                            usageStart.setTime(sdf.parse(thisUsage.getUsedFrom()));
                            usageEnd.setTime(sdf.parse(thisUsage.getUsedTo()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        //ensure that the usage times fall within the time of the booking
                        if (usageStart.after(startDate) && usageEnd.before(endDate)) {
                            //determine how long the room was in use for
                            long timeElapsed = (usageEnd.getTimeInMillis() - usageStart.getTimeInMillis()) / 1000;
                            //add the elapsed time as time that the room was booked and in use
                            usageTime += timeElapsed;
                            //add this usage to the list of usages for this booking
                            reportUsages.add(thisUsage);
                        }
                    }
                }
                //create a new usage report once all usages for this booking have been examined
                report = new UsageReport(bookingTotalTime, usageTime);
                //add the usage report to the list of reports
            }
        }
        return report;
    }
}
