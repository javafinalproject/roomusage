package com.cis2232.roomusage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: Jon Beharrell/Philip Parke
 * Date: November 27, 2014
 * For: CIS-2232
 * Sensor Controller
 * Controller for the Sensor model
 * Responsible for REST routes to access sensors.
 */
@RestController
@RequestMapping(value="/api/v1")
public class SensorController {

    @Autowired
    CrudRepository<Sensor, Long> repository;

    @Autowired
    AuthService authService;


    /**
     * Index
     * Show all sensors.
     * @return
     */
    @RequestMapping(value="/sensors", method=RequestMethod.GET)
    public List<Sensor> index(@RequestHeader(value="Authorization") String token)
    {
        if(authService.verifyToken(token)) {
            List<Sensor> sensorList = new ArrayList<Sensor>();
            for (Sensor item : repository.findAll()) {
                sensorList.add(item);
            }
            return sensorList;
        }
        else
            throw new NotAuthorizedException();


    }

    /**
     * Show
     * Show a specified sensor.
     * @param id
     * @return the sensor that was found
     */
    @RequestMapping(value="/sensors/{id}", method=RequestMethod.GET)
    public Sensor show(@PathVariable long id, @RequestHeader(value="Authorization") String token) {

        if(authService.verifyToken(token))
            return repository.findOne(id);
        else
            throw new NotAuthorizedException();


    }

    /**
     * Store
     * Create a new sensor.
     * @param sensor
     * @return the sensor that was created
     */
    @RequestMapping(value="/sensors", method=RequestMethod.POST)
    public Sensor store(@RequestBody Sensor sensor, @RequestHeader(value="Authorization") String token)
    {
        if(authService.verifyToken(token)) {
            return repository.save(sensor);
        } else {
            throw new NotAuthorizedException();
        }
    }

    /**
     * Update
     * Update an existing sensor.
     * @param sensor
     * @return the location of the sensor as a uri.
     */
    @RequestMapping(value="/sensors/{id}", method=RequestMethod.POST, headers = "Accept=application/json")
    public Map<String,String> update(@PathVariable long id, @RequestBody Sensor sensor, @RequestHeader(value="Authorization") String token)
    {
        Map<String,String> response = new HashMap<>();
        response.put("status", "");

        if(authService.verifyToken(token)) {
            // Get the existing sensor
            Sensor existingSensor = repository.findOne(id);

            // Update the sensor
            existingSensor.update(sensor.getSensorIdentifier(), sensor.getRoomId());

            // Save the changes in the DB
            repository.save(existingSensor);

            response.put("status","OK");
        } else {
            throw new NotAuthorizedException();
        }

        return response;

    }

    /**
     * Delete
     * Delete an existing sensor.
     * @param id
     * @return
     */
    @RequestMapping(value="/sensors/{id}", method=RequestMethod.DELETE)
    public Map<String,String> delete(@PathVariable long id, @RequestHeader(value="Authorization") String token)
    {
        Map<String,String> response = new HashMap<>();
        response.put("status", "");

        if(authService.verifyToken(token)) {
            // Delete the sensor
            repository.delete(id);

            response.put("status","OK");
        } else {
            throw new NotAuthorizedException();
        }

        return response;
    }


}
