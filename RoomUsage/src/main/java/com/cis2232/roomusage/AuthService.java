package com.cis2232.roomusage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Author: Philip Parke
 * Date: November 21, 2014
 * For: CIS-2232
 * Auth Service
 * Holds methods used during the authorization process.
 */
@Service
public class AuthService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    SessionRepository sessionRepository;

    private static final int TOKEN_BYTE_SIZE = 16;

    public String[] decodeBasicAuth(String auth){
        byte[] decoded;

        decoded = Base64.decodeBase64(auth.substring(6));

        // Convert byte array to string
        String d = new String(decoded);

        System.out.println("Decoded: " + d);

        // Split into username and password
        String parts[] = d.split(":", 2);

        return parts;
    }

    /**
     * Generate Token
     * Generate a new token
     * @return
     */
    public Map<String, String> generateToken(){

        // used for mapping objects to json
        ObjectMapper mapper = new ObjectMapper();

        // holds the token and gets converted to json
        Map<String, String> token = new HashMap<String,String>();
        token.put("token", "");

        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[TOKEN_BYTE_SIZE];
        random.nextBytes(salt);

        try {

            token.put("token", PasswordHash.createHash(new String(salt)));

        } catch (NoSuchAlgorithmException ex) {
            System.out.println("Fatal Error: No such algorithm\n" + ex.getMessage());
            System.exit(1);
        } catch (InvalidKeySpecException ex) {
            System.out.println("Fatal Error: Invalid key spec\n" + ex.getMessage());
            System.exit(1);
        }

        return token;
    }


    /**
     * Verify Token
     * Verify the given token against the value stored in the session table.
     * Return true if matches, false otherwise.
     * @param token
     * @return
     */
    public boolean verifyToken(String token){

        // Split into username and token
        String parts[] = decodeBasicAuth(token);

        // Lookup the user by username
        User user = userRepository.findByUsername(parts[0]);

        // If the user was found
        if(user != null) {
            // Lookup the user session by user id
            Session session = sessionRepository.findByUserId(user.getId());

            // If a session was found
            if (session != null) {

                // Get the current date-time in mysql format
                Calendar now = Calendar.getInstance();
                Calendar expires = Calendar.getInstance();
                // Mysql format
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                // Get the expire date as
                try {
                    expires.setTime(sdf.parse(session.getExpires()));
                } catch (java.text.ParseException ex){
                    System.out.println("Date Parse Exception");
                    return false;
                }

                // Compare the two dates
                // return false if the date is greater than
                // or equal to the expire date
                if(now.compareTo(expires) >= 0){
                    return false;
                }

                System.out.println("Found Session: " + session.getToken());
                // Validate the token and return true on success
                if (parts[1].equals(session.getToken())) {
                    return true;
                }
            }
        }

        return false;
    }
}
