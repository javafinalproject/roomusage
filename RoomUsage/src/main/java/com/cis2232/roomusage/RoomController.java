package com.cis2232.roomusage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: Philip Parke
 * Date: November 17, 2014
 * For: CIS-2232
 * Room Controller
 * Controller for the Room model
 * Responsible for REST routes to access rooms.
 */
@RestController
@RequestMapping(value="/api/v1")
public class RoomController {

    @Autowired
    CrudRepository<Room, Long> repository;

    @Autowired
    AuthService authService;


    /**
     * Index
     * Show all rooms.
     * @return
     */
    @RequestMapping(value="/rooms", method=RequestMethod.GET)
    public List<Room> index(@RequestHeader(value="Authorization") String token)
    {
        if(authService.verifyToken(token)) {
            List<Room> rooms_list = new ArrayList<Room>();
            for (Room item : repository.findAll()) {
                rooms_list.add(item);
            }
            return rooms_list;
        }
        else
            throw new NotAuthorizedException();


    }

    /**
     * Show
     * Show a specified room.
     * @param id
     * @return the room that was found
     */
    @RequestMapping(value="/rooms/{id}", method=RequestMethod.GET)
    public Room show(@PathVariable long id, @RequestHeader(value="Authorization") String token) {

        if(authService.verifyToken(token))
            return repository.findOne(id);
        else
            throw new NotAuthorizedException();


    }

    /**
     * Store
     * Create a new room.
     * @param room
     * @return the room that was created
     */
    @RequestMapping(value="/rooms", method=RequestMethod.POST)
    public Room store(@RequestBody Room room, @RequestHeader(value="Authorization") String token)
    {
        if(authService.verifyToken(token)) {
            return repository.save(room);
        } else {
            throw new NotAuthorizedException();
        }
    }

    /**
     * Update
     * Update an existing room.
     * @param room
     * @return the location of the room as a uri.
     */
    @RequestMapping(value="/rooms/{id}", method=RequestMethod.POST, headers = "Accept=application/json")
    public Room update(@PathVariable long id, @RequestBody Room room, @RequestHeader(value="Authorization") String token)
    {
        if(authService.verifyToken(token)) {
            // Get the existing user
            Room existingRoom = repository.findOne(id);

            // Update the room
            existingRoom.update(room.getRoomNumber(), room.getCampus(), room.getRoomType(), room.getSeats(), room.getDescription());

            // Save the changes in the DB
            repository.save(existingRoom);

            return existingRoom;
        } else {
            throw new NotAuthorizedException();
        }

    }

    /**
     * Delete
     * Delete an existing room.
     * @param id
     * @return
     */
    @RequestMapping(value="/rooms/{id}", method=RequestMethod.DELETE)
    public Map<String,String> delete(@PathVariable long id, @RequestHeader(value="Authorization") String token)
    {
        Map<String,String> response = new HashMap<String,String>();
        response.put("status", "");

        if(authService.verifyToken(token)) {

            //Room room = repository.findOne(id);
            // Delete the room
            repository.delete(id);

            response.put("status", "OK");

        } else {
            throw new NotAuthorizedException();
        }

        return response;


    }


}
