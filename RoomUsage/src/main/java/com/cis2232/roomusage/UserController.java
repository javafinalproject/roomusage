package com.cis2232.roomusage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: Philip Parke
 * Date: November 17, 2014
 * For: CIS-2232
 * User Controller
 * Controller for the User model
 * Responsible for REST routes to access users.
 */
@RestController
@RequestMapping(value="/api/v1")
public class UserController {

    @Autowired
    CrudRepository<User, Long> repository;

    @Autowired
    AuthService authService;

    /**
     * Index
     * Show all users.
     * @return
     */
    @RequestMapping(value="/users", method= RequestMethod.GET)
    public List<User> index(@RequestHeader(value="Authorization") String token)
    {
        if(authService.verifyToken(token)) {
            List<User> user_list = new ArrayList<User>();
            for (User user : repository.findAll()) {
                user.hidePassword();
                user_list.add(user);
            }
            return user_list;
        }else{
            throw new NotAuthorizedException();
        }
    }

    /**
     * Show
     * Show a specified user.
     * @param id
     * @return the user that was found
     */
    @RequestMapping(value="/users/{id}", method=RequestMethod.GET)
    public User show(@PathVariable long id, @RequestHeader(value="Authorization") String token) {
        if(authService.verifyToken(token)) {
            User user = repository.findOne(id);
            user.hidePassword();
            return user;
        } else {
            throw new NotAuthorizedException();
        }
    }

    /**
     * Store
     * Create a new user.
     * @param user
     * @return the room that was created
     */
    @RequestMapping(value="/users", method=RequestMethod.POST, headers = "Accept=application/json")
    public User store(@RequestBody User user, @RequestHeader(value="Authorization") String token)
    {
        return repository.save(user);
        /*
        if(authService.verifyToken(token)) {
            return repository.save(user);

        } else {
            throw new NotAuthorizedException();
        }
        */
    }

    /**
     * Update
     * Update an existing user.
     * @param user
     * @return the location of the user as a uri.
     */
    @RequestMapping(value="/users/{id}", method=RequestMethod.POST, headers = "Accept=application/json")
    public Map<String,String> update(@PathVariable long id, @RequestBody User user, @RequestHeader(value="Authorization") String token)
    {
        Map<String,String> response = new HashMap<String,String>();
        response.put("status", "");

        if(authService.verifyToken(token)) {
            // Get the existing user
            User existingUser = repository.findOne(id);

            existingUser.update(user.getUsername(), user.getFirstname(), user.getLastname(), user.getPassword());

            repository.save(existingUser);

            response.put("status","OK");
        } else {
            throw new NotAuthorizedException();
        }

        return response;
    }

    /**
     * Delete
     * Delete an existing user.
     * @param id
     * @return
     */
    @RequestMapping(value="/users/{id}", method=RequestMethod.DELETE)
    public Map<String,String> delete(@PathVariable long id, @RequestHeader(value="Authorization") String token)
    {
        Map<String,String> response = new HashMap<>();
        response.put("status", "");

        if(authService.verifyToken(token)) {
            // Delete the user
            repository.delete(id);

            response.put("status","OK");
        } else {
            throw new NotAuthorizedException();
        }

        return response;
    }
}
