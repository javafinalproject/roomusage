package com.cis2232.roomusage;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
/**
 * Author: Philip Parke
 * Date: November 2, 2014
 * For: CIS-2232
 * User Repository
 * Interface for Crud Repository
 * Any custom accessors defined here.
 * The method names correspond to the properties on the model
 * so findByFirstname will look for the firstname property,
 * case matters!
 */
@RepositoryRestResource
public interface UserRepository extends CrudRepository<User, Long> {

    User findByUsername(String username);

    List<User> findByFirstname(String firstname);

    List<User> findByLastname(String lastname);

}
