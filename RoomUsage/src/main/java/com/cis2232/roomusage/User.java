package com.cis2232.roomusage;

import org.springframework.beans.InvalidPropertyException;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import org.springframework.beans.InvalidPropertyException;

/**
 * Author: Philip Parke
 * Date: November 17, 2014
 * For: CIS-2232
 * User
 * Model for users
 * Represents users table on database.
 */
@Entity
@Table(name="users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @NotNull
    @Column(name = "username")
    private String username;

    @NotNull
    @Column(name = "firstname")
    private String firstname;

    @NotNull
    @Column(name = "lastname")
    private String lastname;

    @NotNull
    @Column(name = "password")
    private String password;


    public long getId(){
        return id;
    }

    public String getUsername(){
        return username;
    }

    public String getFirstname(){
        return firstname;
    }

    public String getLastname(){
        return lastname;
    }

    public String getPassword(){
        return password;
    }

    public void setUsername(String username) throws InvalidPropertyException{
        // Do not allow empty or truncated usernames
        if(username.length() == 0)
            throw new InvalidPropertyException(this.getClass(), "username", "Username cannot be empty.");
        else if(username.length() > 30)
            throw new InvalidPropertyException(this.getClass(), "username", "Username cannot be longer than 30 characters.");
        else
            this.username = username;
    }

    public void setFirstname(String firstname){
        if(firstname.length() == 0)
            this.firstname = "";
        else if(firstname.length() > 30)
            this.firstname = firstname.substring(0, 30);
        else
            this.firstname = firstname;
    }

    public void setLastname(String lastname){
        if(lastname.length() == 0)
            this.lastname = "";
        else if(lastname.length() > 30)
            this.lastname = lastname.substring(0, 30);
        else
            this.lastname = lastname;
    }

    public void setPassword(String password) throws InvalidPropertyException{
        if(password.length() == 0)
            throw new InvalidPropertyException(this.getClass(), "password", "Password cannot be empty.");
        else if(password.length() > 128)
            throw new InvalidPropertyException(this.getClass(), "password", "Password cannot be longer than 128 characters.");
        else
            this.password = hashPassword(password);
    }

    public void hidePassword(){
        this.password = "";
    }

    /**
     * Default Ctor
     * A default constructor is required for jpa to work with the database.
     */
    User(){
        id = 0;
        username = "defaultuser";
        firstname = "first";
        lastname = "last";
        password = "";
    }

    /**
     * Ctor
     * Create a new user with the provided values.
     * @param username
     * @param firstname
     * @param lastname
     * @param password
     * @throws InvalidPropertyException
     */
    User(String username, String firstname, String lastname, String password) throws InvalidPropertyException{

        setUsername(username);
        setFirstname(firstname);
        setLastname(lastname);
        setPassword(password);
    }

    /**
     * Hash Password
     * Pass the plaintext to the PasswordHash.createHash method.
     * Catch deadly errors and exit.
     * @param plaintext
     * @return
     */
    String hashPassword(String plaintext){
        String hash = "";
        try {
            hash = PasswordHash.createHash(plaintext);
        } catch (NoSuchAlgorithmException ex){
            System.out.println("Could not load hash algorithm, quitting...");
            System.out.println(ex.getMessage());
            System.exit(1);
        } catch (InvalidKeySpecException ex){
            System.out.println("Invalid key spec, quitting...");
            System.out.println(ex.getMessage());
            System.exit(1);
        }

        return hash;
    }

    /**
     * Update
     * Update the user with the provided values.
     * @param username
     * @param firstname
     * @param lastname
     * @param password
     */
    void update(String username, String firstname, String lastname, String password)
    {
        if(!username.equals(""))
            this.username = username;
        if(!firstname.equals(""))
            this.firstname = firstname;
        if(!lastname.equals(""))
            this.lastname = lastname;
        if(!password.equals(""))
            this.password = password;
    }



}
