package com.cis2232.roomusage;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Author: Jon Beharrell
 * Date: November 25, 2014
 * For: CIS-2232
 * Sensor Repository
 * Interface for Crud Repository
 * Any custom accessors defined here.
 */
@RepositoryRestResource
public interface SensorRepository extends CrudRepository<Sensor, Long> {

    List<Sensor> findByRoomId(long roomId);

    List<Sensor> findBySensorIdentifier(String sensorIdentifier);
}
