package com.cis2232.roomusage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: Logan Noonan
 * Date: November 19, 2014
 * For: CIS-2232
 * Booking Controller
 * Controller for the Booking model
 * Responsible for REST routes to access bookings.
 */
@RestController
@RequestMapping(value="/api/v1")
public class BookingController {

    private static Iterable<Booking> bookings;

    @Autowired
    CrudRepository<Booking, Long> repository;

    @Autowired
    AuthService authService;


    /**
     * Index
     * Show all bookings.
     * @return
     */
    @RequestMapping(value="/bookings", method=RequestMethod.GET)
    public List<Booking> index(@RequestHeader(value="Authorization") String token)
    {
        if(authService.verifyToken(token)) {
            List<Booking> bookings_list = new ArrayList<Booking>();
            for (Booking item : repository.findAll()) {
                bookings_list.add(item);
            }
            return bookings_list;
        }else
            throw new NotAuthorizedException();
    }

    /**
     * Show
     * Show a specified booking.
     * @param id
     * @return the booking that was found
     */
    @RequestMapping(value="/bookings/{id}", method=RequestMethod.GET)
    public Booking show(@PathVariable long id, @RequestHeader(value="Authorization") String token) {
        if(authService.verifyToken(token)) {
            return repository.findOne(id);
        }else{
            throw new NotAuthorizedException();
        }
    }

    /**
     * Store
     * Create a new booking.
     * @param booking
     * @return the booking that was created
     */
    @RequestMapping(value="/bookings", method=RequestMethod.POST)
    public Booking store(@RequestBody Booking booking, @RequestHeader(value="Authorization") String token)
    {
        if(authService.verifyToken(token)) {
            System.out.println(booking.getBookedFrom());
            Booking newBooking = repository.save(booking);

            return newBooking;
        }else{
            throw new NotAuthorizedException();
        }
    }

    /**
     * Update
     * Update an existing booking.
     * @param id
     * @param booking
     * @param token
     * @return
     */
    @RequestMapping(value="/bookings/{id}", method=RequestMethod.POST, headers = "Accept=application/json")
    public Booking update(@PathVariable long id, @RequestBody Booking booking, @RequestHeader(value="Authorization") String token)
    {

        if(authService.verifyToken(token)) {
            // Get the existing user
            Booking existingBooking = repository.findOne(id);

            // Update the room
            existingBooking.update(booking.getRoomId(), booking.getBookedFrom(), booking.getBookedTo(), booking.getBookedBy(), booking.getDescription());

            return repository.save(existingBooking);

        }else{
            throw new NotAuthorizedException();
        }


    }

    /**
     * Delete
     * Delete an existing room.
     * @param id
     * @return
     */
    @RequestMapping(value="/bookings/{id}", method=RequestMethod.DELETE)
    public Map<String,String> delete(@PathVariable long id, @RequestHeader(value="Authorization") String token)
    {
        Map<String,String> response = new HashMap<>();
        response.put("status", "");

        if(authService.verifyToken(token)) {
            // Delete the room
            repository.delete(id);

            response.put("status","OK");
        }else{
            throw new NotAuthorizedException();
        }

        return response;
    }



}
