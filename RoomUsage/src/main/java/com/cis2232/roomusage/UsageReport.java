package com.cis2232.roomusage;

import java.text.DecimalFormat;

/**
 * Author: Logan Noonan
 * Date: December 10, 2014
 * For: CIS-2232
 * UsageReport
 * Class for UsageReport
 * Holds information about a UsageReport.
 */
public class UsageReport {

    private String totalBookingTime;
    private String totalUsageTime;
    private String usagePercentage;

    public UsageReport(){

    }
    /**
     * Constructor for creating a new usage report.
     *
     * @param totalBookingTime
     * @param totalUsageTime
     */
    public UsageReport(double totalBookingTime, double totalUsageTime){

        this.totalBookingTime = formatTime(totalBookingTime);
        this.totalUsageTime = formatTime(totalUsageTime);
        //get the percentage of time that the booking was used
        float percentage = (float) (totalUsageTime * 100 / totalBookingTime);
        DecimalFormat df = new DecimalFormat("#.00");
        this.usagePercentage = df.format(percentage);

    }
    /**
     * Convert the usage report to a string separated by ascii formatting characters
     * @return
     */
    public String toString(){

        return String.format("Total Booked Time: %d\nTotal Usage Time: %s\nUsage Percentage: %s\n",totalBookingTime,totalUsageTime,usagePercentage);

    }

    public String formatTime(double seconds){
        String time;
        double doubleHours = seconds / 3600;
        //convert hours to whole number, rounded down
        int hours = (int) doubleHours;
        //determine remainder of total hours - hours rounded down to determine minutes
        int minutes = (int)((doubleHours - (double) hours) * 60);
        if (minutes<10) {
            time = hours + "h0" + minutes +"m";
        } else {
            time = hours + "h" + minutes +"m";
        }
        //build the time string
        return time;
    }
    //class getters

    public String getTotalBookingTime(){ return totalBookingTime; }
    public String getTotalUsageTime(){ return totalUsageTime; }
    public String getUsagePercentage(){ return usagePercentage; }
}
