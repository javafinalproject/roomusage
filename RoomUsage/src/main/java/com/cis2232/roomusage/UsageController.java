package com.cis2232.roomusage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: Jon Beharrell / Philip Parke / Logan Noonan
 * Date: December 9, 2014
 * For: CIS-2232
 * Usage Controller
 * Controller for the Usage model
 * Responsible for REST routes to access usages.
 */
@RestController
@RequestMapping(value="/api/v1")
public class UsageController {

    @Autowired
    CrudRepository<Usage, Long> repository;

    @Autowired
    AuthService authService;


    /**
     * Index
     * Show all usages.
     * @return
     */
    @RequestMapping(value="/usages", method=RequestMethod.GET)
    public List<Usage> index(@RequestHeader(value="Authorization") String token)
    {
        if(authService.verifyToken(token)) {
            List<Usage> usageList = new ArrayList<Usage>();
            for (Usage item : repository.findAll()) {
                usageList.add(item);
            }
            return usageList;
        }
        else
            throw new NotAuthorizedException();


    }

    /**
     * Show
     * Show a specified usage.
     * @param id
     * @return the usage that was found
     */
    @RequestMapping(value="/usages/{id}", method=RequestMethod.GET)
    public Usage show(@PathVariable long id, @RequestHeader(value="Authorization") String token) {

        if(authService.verifyToken(token))
            return repository.findOne(id);
        else
            throw new NotAuthorizedException();


    }

    /**
     * Store
     * Create a new usage.
     * @param usage
     * @return the usage that was created
     */
    @RequestMapping(value="/usages", method=RequestMethod.POST)
    public Usage store(@RequestBody Usage usage, @RequestHeader(value="Authorization") String token)
    {
        if(authService.verifyToken(token)) {
            return repository.save(usage);
        } else {
            throw new NotAuthorizedException();
        }
    }

    /**
     * Update
     * Update an existing usage.
     * @param usage
     * @return the location of the usage as a uri.
     */
    @RequestMapping(value="/usages/{id}", method=RequestMethod.POST, headers = "Accept=application/json")
    public Usage update(@PathVariable long id, @RequestBody Usage usage, @RequestHeader(value="Authorization") String token)
    {
        if(authService.verifyToken(token)) {
            // Get the existing usage
            Usage existingUsage = repository.findOne(id);

            // Update the usage
            existingUsage.update(usage.getUsedFrom(), usage.getUsedTo(), usage.getRoomId());

            // Save the changes in the DB
            return repository.save(existingUsage);
        } else {
            throw new NotAuthorizedException();
        }

    }

    /**
     * Delete
     * Delete an existing usage.
     * @param id
     * @return
     */
    @RequestMapping(value="/usages/{id}", method=RequestMethod.DELETE)
    public Map<String,String> delete(@PathVariable long id, @RequestHeader(value="Authorization") String token)
    {
        Map<String,String> response = new HashMap<>();
        response.put("status", "");

        if(authService.verifyToken(token)) {
            // Delete the usage
            repository.delete(id);

            response.put("status","OK");
        } else {
            throw new NotAuthorizedException();
        }

        return response;
    }


}
