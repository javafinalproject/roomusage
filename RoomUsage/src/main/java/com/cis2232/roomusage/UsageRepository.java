package com.cis2232.roomusage;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Author: Jon Beharrell
 * Date: November 25, 2014
 * For: CIS-2232
 * Usage Repository
 * Interface for Crud Repository
 * Any custom accessors defined here.
 */
@RepositoryRestResource
public interface UsageRepository extends CrudRepository<Usage, Long> {
    //finds all usages where from date AND to date fall within between the specified dates
    List <Usage> findByUsedFromBetweenAndUsedToBetween(String fromDate1,String toDate1, String fromDate2,String toDate2);

}
