package com.cis2232.roomusage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Author: Philip Parke
 * Date: December 4, 2014
 * For: CIS-2232
 * File Upload Controller
 * Manages routes for file uploads over the web.
 */
@Controller
public class FileUploadController {

    @Autowired
    ImportService importService;

    /**
     * Handle File Upload
     * This is the upload route to be used when uploading
     * csv files that are to be loaded into the DB
     *
     * @param file  the file posted to the route
     * @return      a message indicating what happened
     */
    @RequestMapping(value="/upload", method = RequestMethod.POST)
    public @ResponseBody String handleFileUpload(@RequestParam("table") String table, @RequestParam("file") MultipartFile file){
        if(!file.isEmpty()){
            try{
                String name = file.getOriginalFilename();

                byte[] bytes = file.getBytes();
                File uploadedFile = new File(name + "-uploaded");
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(uploadedFile));
                stream.write(bytes);
                stream.close();
                System.out.println("File uploaded");

                switch (table) {
                    case "sensors":
                        importService.addSensorsFromCsv(uploadedFile.getPath());
                        break;

                    case "rooms":
                        importService.addRoomsFromCsv(uploadedFile.getPath());
                        break;

                    case "usages":
                        importService.addUsagesFromCsv(uploadedFile.getPath());
                        break;

                    case "bookings":
                        importService.addBookingsFromCsv(uploadedFile.getPath());
                        break;
                }

                return "File upload successful";

            } catch (IOException ex){
                System.out.println("IO Exception\n" + ex.getMessage());
                return "File upload failed.";
            }
        } else {
            return "File upload failed, file was empty";
        }
    }

    /**
     * This method populates a CSV file and returns it
     *
     * @param table Database table to export
     * @return CSV file
     */
    @RequestMapping(value="/export", method = RequestMethod.POST)
    public @ResponseBody FileSystemResource handleExport(@RequestParam("table") String table){

        String fileName = "";

        switch (table) {
            case "sensors":
                fileName = importService.sensorsToCSV();
                break;

            case "rooms":
                fileName = importService.roomsToCSV();
                break;

            case "usages":
                fileName = importService.usagesToCSV();
                break;

            case "bookings":
                fileName = importService.bookingsToCSV();
                break;
        }

        return new FileSystemResource(fileName);
    }
}
