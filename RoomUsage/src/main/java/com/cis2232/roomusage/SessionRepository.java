package com.cis2232.roomusage;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Author: Philip Parke
 * Date: November 20, 2014
 * For: CIS-2232
 * Session Repository
 * Interface for Crud Repository
 * Any custom accessors defined here.
 * The method names correspond to the properties on the model
 * so findByRoomNumber will look for the roomNumber property,
 * case matters!
 */
@RepositoryRestResource
public interface SessionRepository extends CrudRepository<Session, Long> {

    Session findByUserId(long id);
}
