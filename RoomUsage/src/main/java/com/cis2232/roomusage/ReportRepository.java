package com.cis2232.roomusage;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Author: Philip Parke/Logan Noonan
 * Date: November 24, 2014
 * For: CIS-2232
 * Report Repository
 * Interface for Crud Repository
 * Any custom accessors defined here.
 */
@RepositoryRestResource
public interface ReportRepository extends CrudRepository<Report, Long> {

}
