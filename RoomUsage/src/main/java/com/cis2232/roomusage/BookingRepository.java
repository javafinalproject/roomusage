package com.cis2232.roomusage;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Author: Philip Parke
 * Date: November 2, 2014
 * For: CIS-2232
 * Room Repository
 * Interface for Crud Repository
 * Any custom accessors defined here.
 */
@RepositoryRestResource
public interface BookingRepository extends CrudRepository<Booking, Long> {
    //finds all bookings where from date AND to date fall within between the specified dates
    List<Booking> findByBookedFromBetweenAndBookedToBetween(String fromDate1, String toDate1, String fromDate2, String toDate2);

}
