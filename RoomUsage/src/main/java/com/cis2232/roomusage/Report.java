package com.cis2232.roomusage;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Author: Logan Noonan/Philip Parke
 * Date: November 24, 2014
 * For: CIS-2232
 * Booking
 * Model for reports
 * Represents reports table on database.
 */
@Entity
@Table(name="reports")
public class Report {
    
    
    
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id")
    private long id;

    @NotNull
    @Column(name="report_type")
    private String reportType;

    @NotNull
    @Column(name="start_date")
    private String startDate;

    @NotNull
    @Column(name="end_date")
    private String endDate;

    @NotNull
    @Column(name="room_id")
    private String roomId;
    
    @NotNull
    @Column(name="total_booking_time")
    private String totalBookingTime;
    
    @NotNull
    @Column(name="total_usage_time")
    private String totalUsageTime;
    
    @NotNull
    @Column(name="usage_percentage")
    private String usagePercentage;

    /**
     * Constructor without params and with default values.
     */
    Report(){
        this.id = 0;
        this.reportType = "report date";
        this.startDate = "0000/00/00 00:00";
        this.endDate="0000/00/00 00:00";
        this.roomId = "room id";
        this.totalBookingTime = "0.0";
        this.totalUsageTime = "0.0";
        this.usagePercentage = "0.0";

    }

    public void setUsageStatistics(UsageReport usageReport){

            totalBookingTime = String.valueOf(usageReport.getTotalBookingTime());
            totalUsageTime = String.valueOf(usageReport.getTotalUsageTime());
            usagePercentage = String.valueOf(usageReport.getUsagePercentage());
            //handle possible null values if no bookings or usages were found
            if(totalBookingTime=="null") totalBookingTime = "0h00m";
            if(totalUsageTime=="null") totalUsageTime = "0h00m";
            if(usagePercentage=="null") usagePercentage = "0.0";

    }

    /**
     * Constructor for creating a new report.
     *
     * @param reportType
     * @param startDate
     * @param endDate
     * @param roomId
     * @param totalBookingTime
     * @param totalUsageTime
     * @param usagePercentage
     */
    Report(String reportType, String startDate, String endDate, String roomId, String totalBookingTime, String totalUsageTime, String usagePercentage) {

        if(reportType.length() == 0)
            this.reportType = "";
        else if(reportType.length() > 255)
            this.reportType = reportType.substring(0, 255);
        else
            this.reportType = reportType;

        if(startDate.length() == 0)
            this.startDate = "";
        else if(startDate.length() > 255)
            this.startDate = startDate.substring(0, 255);
        else
            this.startDate = startDate;

        if(endDate.length() == 0)
            this.endDate = "";
        else if(endDate.length() > 255)
            this.endDate = endDate.substring(0, 255);
        else
            this.endDate = endDate;

        if(roomId.length() == 0)
            this.roomId = "";
        else if(roomId.length() > 255)
            this.roomId = roomId.substring(0, 255);
        else
            this.roomId = roomId;

        if(totalBookingTime.length() == 0)
            this.totalBookingTime = "";
        else if(totalBookingTime.length() > 255)
            this.totalBookingTime = totalBookingTime.substring(0, 255);
        else
            this.totalBookingTime = totalBookingTime;

        if(totalUsageTime.length() == 0)
            this.totalUsageTime = "";
        else if(totalUsageTime.length() > 255)
            this.totalUsageTime = totalUsageTime.substring(0, 255);
        else
            this.totalUsageTime = totalUsageTime;

        if(usagePercentage.length() == 0)
            this.usagePercentage = "";
        else if(usagePercentage.length() > 255)
            this.usagePercentage = usagePercentage.substring(0, 255);
        else
            this.usagePercentage = usagePercentage;

    }

    // Getters and setters
    public long getId() {
        return id;
    }
    public String getReportType() {
        return reportType;
    }
    public String getStartDate() {
        return startDate;
    }
    public String getEndDate() {
        return endDate;
    }
    public String getRoomId() {
        return roomId;
    }
    public String getTotalBookingTime(){
        return totalBookingTime;
    }
    public String getTotalUsageTime(){
        return totalUsageTime;
    }
    public String getUsagePercentage(){
        return usagePercentage;
    }

    /**
     * Convert the booking to a string separated by ascii formatting characters
     * @return
     */
    public String toString(){
        return String.format("ID: %d\nRoom ID: %s\nReport Type: %s\nStart Date: %s\nEnd Date: %s\n Total Booked Time: %s\n Total Usage Time: %s\n Usage Percentage: %s", id, roomId, reportType, startDate, endDate, totalBookingTime, totalUsageTime, usagePercentage);
    }
    /**
     * Update this report.
     * @param reportType
     * @param startDate
     * @param endDate
     * @param roomId
     * @param totalBookingTime
     * @param totalUsageTime
     * @param usagePercentage
     */
    void update(String reportType, String startDate, String endDate, String roomId, String totalBookingTime, String totalUsageTime, String usagePercentage)
    {

        if(!reportType.equals(""))
            this.reportType = reportType;
        if(!startDate.equals(""))
            this.startDate = startDate;
        if(!endDate.equals(""))
            this.endDate= endDate;
        if(!roomId.equals(""))
            this.roomId= roomId;
        if(!totalBookingTime.equals("")){
            this.totalBookingTime= totalBookingTime;
        }
        if(!totalUsageTime.equals("")){
            this.totalUsageTime= totalUsageTime;
        }
        if(!usagePercentage.equals("")){
            this.usagePercentage= usagePercentage;
        }

    }

}

