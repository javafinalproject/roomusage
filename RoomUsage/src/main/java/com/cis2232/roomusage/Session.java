package com.cis2232.roomusage;

import org.apache.tomcat.util.codec.binary.Base64;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Author: Philip Parke
 * Date: November 20, 2014
 * For: CIS-2232
 * Room
 * Model for rooms
 * Represents rooms table on database.
 */
@Entity
@Table(name="sessions")
public class Session {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @NotNull
    @Column(name = "user_id")
    private long userId;
    

    @NotNull
    @Column(name = "token")
    private String token;

    @NotNull
    @Column(name = "expires")
    private String expires;

    /**
     * Constructor without params and with default values.
     * Require for jpa to work with the database.
     */
    Session(){
        this.id = 0;
        this.userId = 0;
        this.token = "0";
        this.expires = "1974-01-01 00:00:00";
    }

    /**
     * Constructor with parameters
     * Create a new session object with the specified params
     * @param userId
     * @param token
     * @param expires
     */
    Session(long userId, String token, String expires){
        this.userId = userId;
        this.token = token;
        this.expires = expires;
    }

    public long getId(){ return id; }
    public long getUserId() { return userId; }
    public String getToken() { return token; }
    public String getExpires() { return expires; }


    public void update(String token, String expires){
        this.token = token;
        this.expires = expires;
    }


}
