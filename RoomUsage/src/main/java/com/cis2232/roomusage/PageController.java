package com.cis2232.roomusage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Author: Philip Parke
 * Date: November 17, 2014
 * For: CIS-2232
 * Page Controller
 * Controller for templating engine pages
 */
@Controller
public class PageController {


    @RequestMapping("/")
    public String index(@RequestParam(value="title", required=false, defaultValue="Room Usage") String title, Model model) {
        // pass in the title parameter
        model.addAttribute("title", title);

        // return the index template
        return "index";
    }
}
