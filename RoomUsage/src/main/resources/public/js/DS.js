
// Local Storage Adapter
DS = function(){

  // If localStorage is implemented by the browser
  if(typeof(Storage) !== undefined){
    // If it contains a models object
    if(localStorage.hasOwnProperty("models")){
      // Retrieve the models data
      this.models = JSON.parse(localStorage.getItem("models"));
    }else{
      // Create a new models object
      this.models = {
        user: {
          username: "",
          token: ""
        }
      };
    }
  }else{
    alert("Your browser does not support local storage, your session will expire when you leave the page.");
  }
};

DS.prototype.get = function(model, key){
  return this.models[model][key];
};

DS.prototype.getTemplateMap = function(model, attr){
  if(attr){
    return this.models[model].templateMap[attr];
  }else{
    return this.models[model].templateMap;
  }
}

DS.prototype.getFormMap = function(model, attr){
  if(attr){
    return this.models[model].formMap[attr];
  }else{
    return this.models[model].formMap;
  }
}

DS.prototype.set = function(model, key, value){
  this.models[model][key] = value;
};

// add a new model to the datastore
DS.prototype.addModel = function(model, templateMap, formMap){
  // only create a new model if it doesn't exist
  if(this.models[model] === undefined){
    this.models[model] = {  size: 0,
                            records: [],
                            templateMap: templateMap || {},
                            formMap: formMap || {},
                            lastUpdated: 0
                         };

  }
};

// store a new record
DS.prototype.store = function(model, record){
  // add to the array of records
  this.models[model].records.push(record);
  // increment size
  this.models[model].size++;

  // set the updated time
  this.models[model].lastUpdated = Date.now();

  // return the id
  return record.id;
};

// clear the current records
DS.prototype.clear = function(model){
  this.models[model].records = [];
  this.models[model].size = 0;
  // set the updated time
  this.models[model].lastUpdated = Date.now();
}

// remove a record by id
DS.prototype.delete = function(model, id){
  var index = 0;
  for(var key in this.models[model].records){
    if(this.models[model].records[key].id === id){
      // remove the element
      this.models[model].records = this.models[model].records.splice(index, 1);
    }
    index++;
  }
};

// find a record by id
DS.prototype.find = function(model, id){
  for(var key in this.models[model].records){
    if(this.models[model].records[key].id === id){
      return this.models[model].records[key];
    }
  }
};

DS.prototype.save = function(){
  localStorage.setItem("models", JSON.stringify(this.models));
};
