Observer = function(){

  // an object containing arrays of callback functions
  // indexed by an event name, each callback has a
  // unique uid
  this.events = {};

  // a counter used to keep track of the currently
  // available uid
  this.currentUid = -1;
};

// Publish events by calling the functions
// associated with the event name, add any
// needed arguments as an array and pass
// the scope in which the function should
// be called
Observer.prototype.publish = function( event, args ){

  if( this.events && this.events[event] ){

    // get the subscribers of this event
    var subscribers = this.events[event];

    // for each of the subscribers to this event
    subscribers.forEach( function( element, index, array ){
      // call the function with the provided scope
      element.func.apply( element.scope, args );
    });

    return this;
  }

  console.error('Observer Error in publish: Event not found, typeof this.events is %s and is%s null', typeof this.events, this.events === null ? '' : ' not');
  return false;
};

// Subscribe to an event using the event
// name and providing a callback function
// to be executed when the event occurs
Observer.prototype.subscribe = function( event, func, scope ){

  if(this.events){

    // If an entry for this event doesn't already exist
    // or is null or some other falsy value
    if( !this.events[event] ){
      // Add a new entry containing an empty array
      this.events[event] = [];
    }

    // get the next uid
    var UID = (++this.currentUid).toString();
    this.events[event].push({
      UID: UID,
      func: func,
      scope: scope
    });
    return UID;
  }

  console.error('Observer Error in subscribe: this.events is ', this.events);
  return false;
};

// Unsubscribe from an event using the
// unique id issued by subscribe
Observer.prototype.unsubscribe = function( UID ){

  // traverse this.events
  for(var event in this.events){
    if(this.events.hasOwnProperty(event)){

      // get the subscribers
      var subscribers = this.events[event];
      var scope = {UID: UID};

      // search for and delete the callback with
      // the specified UID, return immediately
      if( subscribers.some(deleteCallback, scope) ){
        return UID;
      }
    }
  }

  // deletes the UID if found and returns true
  // triggering the end of the search
  function deleteCallback( element, index, array ){
    if(element.UID === this.UID){
      delete array[index];
      return true;
    }

    return false;
  }

  // return false if the UID was not found
  return false;
};
