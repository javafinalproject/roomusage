
Request = function(url, api){

  this.url = url || '';
  this.api = api || '';
  this.credentials = {username: '', password: ''};

}

Request.prototype.setCredentials = function(username, password){
  this.credentials.username = username;
  this.credentials.password = password;
}

Request.prototype.get = function(path, data, callback){
  this.apiCall(path, data, callback, 'GET');
}

Request.prototype.post = function(path, data, callback){
  this.apiCall(path, data, callback, 'POST');
}

// Make a call to the api
// data should be an object that can be parsed as json
// either a plain js object or if formdata, call serializeArray()
// on it first before passing in
Request.prototype.apiCall = function(path, data, callback, method, auth)
{
  console.log('API Call');
  console.log(data);
  console.log(path);

  var keyValue = auth ? auth : {username: this.credentials.username, password: this.credentials.password};

  // Query
  $.ajax({
    type: method ? method : 'POST',
    url: this.url + this.api + path,
    data: JSON.stringify(data),
    dataType: 'json',
    headers: {
      "Authorization": "Basic " + btoa(keyValue.username + ":" + keyValue.password)
    },
    processData:  true,
    contentType:  "application/json; charset=utf-8",

    beforeSend: function(xhr) {
      xhr.setRequestHeader("Accept", "application/json");
      xhr.setRequestHeader("Content-Type", "application/json");
    },

    success: function(data){

      callback(data);
    },

    // Error
    error: function(data){
      console.log('Ajax error');
      console.log(data);
      console.log(path);
      if(typeof callbacks === 'object' && typeof callbacks.error === 'function')
        callbacks.error(data);
      }
    });

}

// Upload a file to the api
// accepts a formdata object
Request.prototype.upload = function(path, data, callback){
  console.log('File Upload');

  var keyValue = auth ? auth : {username: this.credentials.username, password: this.credentials.password};

  // Query
  $.ajax({
    type: 'POST',
    url: this.url + this.api + path,
    data: JSON.stringify(data),
    dataType: 'json',
    headers: {
      "Authorization": "Basic " + btoa(keyValue.username + ":" + keyValue.password)
    },
    processData: data.constructor.name == "FormData" ? false : true,
    contentType:  "multipart/form-data",
/*
    beforeSend: function(xhr) {
      xhr.setRequestHeader("Accept", "application/json");
      xhr.setRequestHeader("Content-Type", "application/json");
    },
*/
    success: function(data){

      callback(data);
    },

    // Error
    error: function(data){
      console.log('Ajax error');
      console.log(data);
      if(typeof callback === 'object' && typeof callback.error === 'function')
        callback.error(data);
      }
    });

}
