//==============================================================================
// Requires:
//  Observer.js
//  DS.js
//  Request.js
//
// Author: Philip Parke
// For: CIS-2232 Room Usage Final Project
// Description:
//    Provides a basic framework for managing client side
//    interface and data transmission.  Observer is used
//    to watch for returned data from api calls, Request
//    performs needed communication with the server, DS
//    provides persistent storage on the client and allows
//    searching, updating, and retrieving data locally.
//    Could be expanded further to provide more features.
//==============================================================================


//==============================================================================
// Transition to a new pane
//==============================================================================
function transition(pane){
    $('.pane').hide();
    $('.'+pane).fadeIn('slow');
}

//==============================================================================
// First Init
//==============================================================================
function init(){
  // create observer
  observer = new Observer();
  // create the datastore
  datastore = new DS();
  // instantiate a new requerst object
  request = new Request('http://localhost:8080', '/api/v1');

  // get the credentials from the datastore
  request.setCredentials(datastore.get('user', 'username'), datastore.get('user', 'token'));

  $('.pane').hide();

  initIndex();
  initLogin();
  initRooms();
  initUsers();
  initBookings();
  initUsages();
  initReports();
  initSensors();

  // action to perform when a new record is added
  observer.subscribe('addrecord', recordAdded, this);

  observer.subscribe('clearform', clearForm, this);

  $('.index-link').click(function(){
      transition('index');
  });

  $('.login-link').click(function(){
      transition('login');
  });

  $('.rooms-link').click(function(){
      transition('rooms');
  });

  $('.users-link').click(function(){
      transition('users');
  });

  $('.bookings-link').click(function(){
      transition('bookings');
  });

  $('.usages-link').click(function(){
    transition('usages');
  });

  $('.reports-link').click(function(){
      transition('reports');
  });

  $('.sensors-link').click(function(){
      transition('sensors');
  });

  $('.upload-link').click(function(){
      transition('upload');
  });

  $('.logout-link').click(function(){
    logout();
    transition('index');
  });

  $('.refresh-link').click(function(){
    updateModels();

  });
}



//==============================================================================
// Init the Index pane
//==============================================================================
function initIndex(){
  // Hide most of the menu if not logged in
  validate();

  $('.index').fadeIn('slow');

}

//==============================================================================
// Init the Login pane
//==============================================================================
function initLogin()
{
    $("#login-submit-btn").click(function(event){
        event.preventDefault();

        var form = $('#login-form');

        var username = form.find('[name=username]').first().val();
        var password = form.find('[name=password]').first().val();

        login(username, password);
    });
}

//==============================================================================
// Attempt to log in
//==============================================================================
function login(username, password){

  var callback = function(data) {
    if(data.token) {
      // store credentials
      datastore.set('user', 'username', username);
      datastore.set('user', 'token', data.token);
      datastore.save();
      // set credentials for further requests
      request.setCredentials(username, data.token);
      // make it appear that the user is logged in
      toggleLoggedIn();
      // update all the models
      updateModels();
    }else{
      alert("Login Failed");
    }
  };

  request.apiCall('/login', {}, callback, 'GET', {username: username, password: password});
}

//==============================================================================
// Validate the current token and username combination
//==============================================================================
function validate(){

  var callback = function(data){
    if(data.status !== 'OK'){
      logout();
    }else{
      // make it appear that the user is logged in
      toggleLoggedIn();
      // update all the models
      updateModels();
    }
  }
    // attempt login with current credentials
    request.get('/validate', {}, callback);

}

//==============================================================================
// Show logged in state
//==============================================================================
function toggleLoggedIn(){
  // show all the links
  $('.link').show();
  // hide the login link
  $('#login-nav').hide();
  // show the username and logout nav
  $('#username-nav').text(datastore.get('user', 'username'));
  $('#logout-nav').show();
  // switch back to the index
  transition('index');
}

//==============================================================================
// Logout
//==============================================================================
function logout(){
  $('.link').hide();

  $('#username-nav').text('');
  $('#logout-nav').hide();

  $('.login-link').show();
  $('.index-link').show();
  $('.users-link').show();

  datastore.set('user', 'username', '');
  datastore.set('user', 'token', '');
  datastore.save();

  $('.item').not('.template').remove();
}

//==============================================================================
// Init the Rooms pane
//==============================================================================
function initRooms()
{
    var templateMap = {
        template: 'room-template',
        list: 'room-list',
        item: 'room-item',
        path: '/rooms',
        'room-number': 'roomNumber',
        campus: 'campus',
        'room-type': 'roomType',
        seats: 'seats',
        description: 'description'
    };


    var formMap = {
        form: 'room-form',
        roomId : 'room-id',
        roomNumber : 'room-number',
        campus : 'campus',
        roomType : 'room-type',
        seats : 'seats',
        description : 'description',
        submitButton : 'rooms-submit-btn',
        updateButton : 'rooms-update-btn',
        deleteButton : 'rooms-delete-btn'
    };

    // add the model to the datastore
    datastore.addModel('rooms', templateMap, formMap);

    // get the current records from the server
    //getRecords('rooms');

    // initialize the buttons
    initButtons('rooms');

    // set the click event to show the update form
    setShowUpdateForm('rooms', $('#'+templateMap.list));

}

//==============================================================================
// Init the Users pane
//==============================================================================
function initUsers()
{
    var templateMap = {
        template: 'user-template',
        list: 'user-list',
        item: 'user-item',
        path: '/users',
        username: 'username',
        firstname: 'firstname',
        lastname: 'lastname'
    };

    var formMap = {
        form: 'user-form',
        username: 'username',
        firstname: 'first-name',
        lastname: 'last-name',
        password: 'password',
        passwordConfirm : 'password-confirm',
        submitButton : 'users-submit-btn',
        updateButton : 'users-update-btn',
        deleteButton : 'users-delete-btn'
    };

    // add the model to the datastore
    datastore.addModel('users', templateMap, formMap);

    // get the current records from the server
    //getRecords('users');

    initButtons('users');

    $('#users-submit-btn').unbind('click').click(function(){
        var form = $('#user-form');

        // check to make sure passwords match
        if(form.find('[name=password]').first().val() !== form.find('[name=password-confirm]').first().val()){
          alert("Password fields do not match.");
            return false;
          }

        submitRecord('users');
    });

    $('#users-update-btn').unbind('click').click(function(){
      var form = $('#user-form');

      // check to make sure passwords match
      if(form.find('[name=password]').first().val() !== form.find('[name=password-confirm]').first().val()){
        alert("Password fields do not match.");
        return false;
      }

      updateRecord('users', $(this).data('uid'));
    });

    // set the click event to show the update form
    setShowUpdateForm('users', $('#'+templateMap.list));
}

//==============================================================================
// Init the Bookings pane
//==============================================================================
function initBookings()
{
    var templateMap = {
        template: 'booking-template',
        list: 'booking-list',
        item: 'booking-item',
        path: '/bookings',
        'room-number': 'roomNumber',
        'booked-from': 'bookedFrom',
        'booked-to': 'bookedTo',
        'booked-by': 'bookedBy',
        description: 'description'
    };

    var formMap = {
        form: 'booking-form',
        roomId : 'room-id',
        bookedFrom : 'booked-from',
        bookedTo : 'booked-to',
        bookedBy : 'booked-by',
        description : 'description',
        submitButton : 'bookings-submit-btn',
        updateButton : 'bookings-update-btn',
        deleteButton : 'bookings-delete-btn'
    };

    // add the model to the datastore
    datastore.addModel('bookings', templateMap, formMap);

    // get the current records from the server
    //getRecords('bookings');

    initButtons('bookings');

    // set the click event to show the update form
    setShowUpdateForm('bookings', $('#'+templateMap.list));
}

//==============================================================================
// Init the Usages pane
//==============================================================================
function initUsages()
{
  var templateMap = {
    template: 'usage-template',
    list: 'usage-list',
    item: 'usage-item',
    path: '/usages',
    'room-number': 'roomNumber',
    'used-from': 'usedFrom',
    'used-to': 'usedTo',
    description: 'description'
  };

  var formMap = {
    form: 'usage-form',
    roomId : 'room-id',
    usedFrom : 'used-from',
    usedTo : 'used-to',
    submitButton : 'usages-submit-btn',
    updateButton : 'usages-update-btn',
    deleteButton : 'usages-delete-btn'
  };

  // add the model to the datastore
  datastore.addModel('usages', templateMap, formMap);

  initButtons('usages');

  // set the click event to show the update form
  setShowUpdateForm('usages', $('#'+templateMap.list));
}

//==============================================================================
// Init the Reports pane
//==============================================================================
function initReports()
{
    var templateMap = {
        template: 'report-template',
        list: 'report-list',
        item: 'report-item',
        path: '/reports',
        'room-number': 'roomNumber',
        'report-type': 'reportType',
        'start-date': 'startDate',
        'end-date' : 'endDate',
        'total-booking-time' : 'totalBookingTime',
        'total-usage-time' : 'totalUsageTime',
        'usage-percentage' : 'usagePercentage'
    };

    var formMap = {
        form: 'report-form',
        reportType : 'report-type',
        startDate : 'start-date',
        endDate : 'end-date',
        roomId : 'room-id',
        submitButton : 'reports-submit-btn',
        deleteButton : 'reports-delete-btn'
    };

    // add the model to the datastore
    datastore.addModel('reports', templateMap, formMap);

    // get the current records from the server
    //getRecords('reports');

    initButtons('reports');

    // set the click event to show the update form
    setShowUpdateForm('reports', $('#'+templateMap.list));
}

//==============================================================================
// Init the Sensors pane
//==============================================================================
function initSensors(){
  var templateMap = {
      template: 'sensor-template',
      list: 'sensor-list',
      item: 'sensor-item',
      path: '/sensors',
      'sensor-identifier': 'sensorIdentifier',
      'room-number': 'roomNumber'
    };

  var formMap = {
      form: 'sensor-form',
      'sensorIdentifier': 'sensor-identifier',
      roomId : 'room-id',
      submitButton : 'sensors-submit-btn',
      updateButton : 'sensors-update-btn',
      deleteButton : 'sensors-delete-btn'
    };

  // add the model to the datastore
  datastore.addModel('sensors', templateMap, formMap);

  // get the current records from the server
  //getRecords('sensors');

  // initialize the buttons
  initButtons('sensors');

  // set the click event to show the update form
  setShowUpdateForm('sensors', $('#'+templateMap.list));
}

//==============================================================================
// Update Models
//==============================================================================
function updateModels(models){
  var models = models || ['rooms', 'users', 'bookings', 'usages', 'reports', 'sensors'];

  // spin the spinner
  var icon = $('.refresh-link').find('.glyphicon');
  icon.addClass('rotate');
  setTimeout(function(){
    icon.removeClass('rotate');
  }, 1000);


  for(var m in models){
    $('.' + models[m].slice(0, -1) + '-item').not('.template').remove();
    // clear the current records
    datastore.clear(models[m]);
    // get records from the server
    getRecords(models[m]);
  }
}

//==============================================================================
// set the click events for the form buttons
//==============================================================================
function initButtons(model){
  var formMap = datastore.getFormMap(model);
  // Update button
  $('#' + formMap.updateButton).unbind('click').click(function(){
    updateRecord(model, $(this).data('uid'));
  });

  // Delete button
  $('#' + formMap.deleteButton).unbind('click').click(function(){
    deleteRecord(model, $(this).data('uid'));
  });

  // Submit button
  $('#' + formMap.submitButton).unbind('click').click(function(){
    submitRecord(model);
  });

  // Clear the form and return to 'new entry' mode
  $('#' + formMap.form + ' .clear-form').unbind('click').click(function(){
    clearForm(model);
    toggleSubmit(model);
  });
}

//==============================================================================
// get records for the model from the server
//==============================================================================
function getRecords(model)
{

    var d = {
    };

    var callback = function(data){
      // publish the event
      observer.publish('addrecord', [model, data]);
        console.log(data);
    };

    // send request to the server
    request.get(datastore.getTemplateMap(model, 'path'), d, callback);
}

//==============================================================================
// submit a new record to the server
//==============================================================================
function submitRecord(model)
{

    var d = {};

    var formMap = datastore.getFormMap(model);

    var form = $('#' + formMap.form);


    for(var key in formMap){
        if(formMap.hasOwnProperty(key)){
            d[key] = form.find('[name=' + formMap[key] + ']').val();
        }
    }

    var callback = function(data){
        // publish the event
        observer.publish('addrecord', [model, data]);
        observer.publish('clearform', [model]);
    };

    request.post(datastore.getTemplateMap(model, 'path'), d, callback);
}

//==============================================================================
// Record Added
// these actions are performed whenever a new record is added
// to the database
//==============================================================================
function recordAdded(model, data){

  // if there's only one record, put it in an array
  if(!(data instanceof Array))
    data = [data];

  // for each record
  for(var key in data){

    // add roomNumber to the record
    if(model === 'bookings' || model === 'sensors' || model === 'usages' || model === 'reports'){
      var roomId = parseInt(data[key].roomId)
      if(roomId !== NaN){
        var room = datastore.find('rooms', roomId);
        if(room){
          data[key]['roomNumber'] = room.roomNumber;
        }else{
          console.log("room not found ", roomId);
          
        }
      }
    }

    // store the record in the datastore
    datastore.store(model, data[key])

    // cause the data to be appended to
    // the appropriate listbox for this model
    appendTemplate(model, data[key]);

    // if a new room was added we need to update
    // the select boxes for rooms
    if(model === 'rooms'){
      populateSelects([{value:data[key].id, text:data[key].roomNumber}], $('[name=room-id]'));
    }


  }
}

//==============================================================================
// set the fields in the form to the matching
// fields in the record
//==============================================================================
function setForm(model, record){

  // local reference to formMap
  var formMap = datastore.getFormMap(model);

  var form = $('#' + formMap.form);

  for(var key in formMap){
    if(formMap.hasOwnProperty(key)){
      if(record.hasOwnProperty(key)){
        form.find('[name=' + formMap[key] + ']').val(record[key]);
      }
    }
  }
}

//==============================================================================
// clear the input form
//==============================================================================
function clearForm(model){

  // local reference to formMap
  var formMap = datastore.getFormMap(model);

  var form = $('#' + formMap.form);

  for(var key in formMap){
    if(formMap.hasOwnProperty(key)){
      form.find('[name=' + formMap[key] + ']').val('');
    }
  }
}

//==============================================================================
// show the button(s) necessary for submitting
//==============================================================================
function toggleSubmit(model){
  var formMap = datastore.getFormMap(model);

  $('#' + formMap.form + ' .clear-form').hide();
  $('#' + formMap.updateButton).hide();
  $('#' + formMap.deleteButton).hide();
  $('#' + formMap.submitButton).show();
}

//==============================================================================
// show the button(s) necessary for updating or deleting
//==============================================================================
function toggleUpdate(model){
  var formMap = datastore.getFormMap(model);

  $('#' + formMap.form + ' .clear-form').show();
  $('#' + formMap.submitButton).hide();
  $('#' + formMap.updateButton).show();
  $('#' + formMap.deleteButton).show();

}

//==============================================================================
// Set the element to show the update form on click
//==============================================================================
function setShowUpdateForm(model, elem){

  // local references to templateMap and formMap
  var templateMap = datastore.getTemplateMap(model);
  var formMap = datastore.getFormMap(model);



  $(elem).unbind('click').click(function(event){

    // get the element that was clicked
    var target = $(event.target);

    var li = target.closest('.item')

    li.css('background-color', 'rgba(0,0,0,0.3)');
    li.siblings('.' + templateMap.item).css('background-color', 'transparent');

    // show the update and delete buttons
    toggleUpdate(model);

    $('#' + formMap.updateButton).data('uid', li.data('uid'));
    $('#' + formMap.deleteButton).data('uid', li.data('uid'));

    // get record by id
    var record = datastore.find(model, li.data('uid'));

    // set the form with values from the record
    setForm(model, record);
  });
}

//==============================================================================
// format data using a templateMap and append to specified list
//==============================================================================
function appendTemplate(model, data){

  // get local reference to templateMap
  var templateMap = datastore.getTemplateMap(model);

  var clone = $('#'+ templateMap.template).clone();

  // set the uid of the element to the id of the record
  clone.data('uid', data['id']);
  clone.removeAttr('id');
  clone.removeClass('template');


  // for each key in the dataMap passed in
  for(var key in templateMap) {
      if(templateMap.hasOwnProperty(key)) {
          // if the same key exists in the returned data
          if (data.hasOwnProperty(templateMap[key])) {
              // set the text of the element with class key to
              // the element in data[i] with key dataMap[key]
              clone.find('.' + key).text(data[templateMap[key]]);

          }
      }
  }

  // unhide the element
  clone.show();

  // append the new element to the display list
  clone.appendTo('#'+templateMap.list);
}

//==============================================================================
// update a record on the server
//==============================================================================
function updateRecord(model, uid){

  // local reference to formMap
  var formMap = datastore.getFormMap(model);

  var d = {};

  var form = $('#' + formMap.form);

  // Add the data from the form
  for(var key in formMap){
    if(formMap.hasOwnProperty(key)){
      d[key] = form.find('[name=' + formMap[key] + ']').val();
    }
  }

  // local reference to templateMap
  var templateMap = datastore.getTemplateMap(model);

  // Finds the element in the list and updates it
  var callback = function(data){
    var c = $('#' + templateMap.list).children();
    var elem = $.grep(c, function(item){return $(item).data('uid') == uid;});
    updateItem(model, data, elem);
  };

  // send the update request to the server
  request.post(templateMap.path + '/' + uid, d, callback);
}

//==============================================================================
// Update an element using provided data and templateMap
//==============================================================================
function updateItem(model, data, item){

  var templateMap = datastore.getTemplateMap(model);
  // for each key in the dataMap passed in
  for(var key in templateMap) {
    if(templateMap.hasOwnProperty(key)) {
      // if the same key exists in the returned data
      if (data.hasOwnProperty(templateMap[key])) {
        // set the text of the element with class key to
        // the element in data[i] with key dataMap[key]
        $(item).find('.' + key).text(data[templateMap[key]])
      }
    }
  }
}

//==============================================================================
// delete a record from the database by id
// remove the element from the display list if
// successful
//==============================================================================
function deleteRecord(model, uid){

  // local reference to templateMap
  var templateMap = datastore.getTemplateMap(model);

  var d = {};

  // Finds the element in the list and deletes it
  var callback = function(data){

    if(data.status === "OK"){
      var c = $('#' + templateMap.list).children();
      var elem = $.grep(c, function(item){return $(item).data('uid') == uid;});
      $(elem).remove();
      observer.publish('clearform', [model]);
      toggleSubmit(model);
    }
    else{
      alert("Delete failed.");
    }
  };

  // send the delete request to the server
  request.apiCall(templateMap.path + '/' + uid, d, callback, 'DELETE');
}

//==============================================================================
// Populate select box
// arr should be an array of objects containing value and text
//==============================================================================
function populateSelects(arr, elems){
  // for each list element
  for(var i = 0; i < elems.length; ++i){
    // for each array element
    for(var e in arr){
      $(elems[i]).append('<option value=' + arr[e].value + '>' + arr[e].text + '</option>');
    }
  }

}
