INSERT INTO `rooms` (`room_number`, `campus`, `room_type`, `seats`, `description`) VALUES ('124', 'Charlottetown', 'Computer Lab', '22', 'White board, Smart board');
INSERT INTO `bookings`(`booked_from`, `booked_to`, `booked_by`, `description`, `room_id`) VALUES ('2014/09/12 09:00','2014/09/12 12:00','John Smith','CIS-2232','1');
INSERT INTO `usages`(`used_from`, `used_to`, `room_id`) VALUES ('2014/09/12 09:01','2014/09/12 09:46','1');
INSERT INTO `usages`(`used_from`, `used_to`, `room_id`) VALUES ('2014/09/12 10:40','2014/09/12 11:00','1');